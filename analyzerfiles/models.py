from django.db import models

# Create your models here.
from django.db import models


class Document(models.Model):
    title = models.CharField(max_length=100)
    url = models.FileField(upload_to='files/')
    date_create = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class FileExcel(models.Model):
    region=models.CharField(max_length=50)
    country=models.CharField(max_length=50)
    item_type=models.CharField(max_length=50)
    sales_channel=models.CharField(max_length=50)
    order_priority=models.CharField(max_length=50)
    order_date=models.DateField()
    order_id=models.IntegerField()
    ship_date=models.DateField()
    units_sold=models.IntegerField()
    unit_price=models.FloatField()
    unit_cost=models.FloatField()
    total_revenue=models.FloatField()
    total_cost=models.FloatField()
    total_profit=models.FloatField()

class FileCsv(models.Model):
    date=models.DateField()
    Description=models.CharField(max_length=50)
    Deposits=models.FloatField()
    Withdrawls=models.FloatField()
    Balance=models.FloatField()